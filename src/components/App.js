import React from 'react';
import '../assets/scss/app';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: false,
      backgroundColorTimer: false,
      transform: false,
    };
    this.handleScroll = this.handleScroll.bind(this);
  }

  componentDidMount() {
    this.timer = window.setInterval(() => {
      this.setState({
        backgroundColorTimer: !this.state.backgroundColorTimer,
      });
    }, 10000);
    window.addEventListener('scroll', this.handleScroll);
  }

  swithState() {
    this.setState({backgroundColor: !this.state.backgroundColor});
  }

  componentWillUnmount() {
    console.log('cool');
    const color = document.getElementsByClassName('color--turquoise');
    color.removeEventListener('click', this.clickListener);
    clearInterval(this.timer);
    clearInterval(this.swithState);
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll(event) {
    const {scrollTop} = event.srcElement.body;
    if (Math.min(0, scrollTop / 3 - 60)) {
      this.setState({
        transform: true,
      });
    }
  }

  // eslint-disable-next-line complexity
  render() {
    return (
      <div className="content">
        <div className="row">
          <div
            onClick={() => this.swithState()}
            className="col-4 color--turquoise"
            style={{
              backgroundColor: this.state.backgroundColor
                ? '#45ADA8'
                : 'turquoise',
            }}
          >
            click me
          </div>
          <div
            className="col-4 color--pink"
            style={{
              backgroundColor: this.state.backgroundColorTimer
                ? '#B38184'
                : 'pink',
            }}
          >
            wait 10 sec
          </div>
          <div
            onScroll={() => this.handleScroll()}
            className="col-4 color--beige"
            style={{
              backgroundColor: this.state.transform ? '#F7E4BE' : 'beige',
            }}
          >
            scroll me
          </div>
        </div>
        <div className="row">
          <div
            onScroll={() => this.handleScroll()}
            className="col-4 color--beige"
            style={{
              backgroundColor: this.state.transform ? '#F7E4BE' : 'beige',
            }}
          >
            scroll me
          </div>{' '}
          <div
            onClick={() => this.swithState()}
            className="col-4 color--turquoise"
            style={{
              backgroundColor: this.state.backgroundColor
                ? '#45ADA8'
                : 'turquoise',
            }}
          >
            click me
          </div>
          <div
            className="col-4 color--pink"
            style={{
              backgroundColor: this.state.backgroundColorTimer
                ? '#B38184'
                : 'pink',
            }}
          >
            wait 10 sec
          </div>{' '}
        </div>
        <div className="row">
          <div
            className="col-4 color--pink"
            style={{
              backgroundColor: this.state.backgroundColorTimer
                ? '#B38184'
                : 'pink',
            }}
          >
            wait 10 sec
          </div>{' '}
          <div
            onScroll={() => this.handleScroll()}
            className="col-4 color--beige"
            style={{
              backgroundColor: this.state.transform ? '#F7E4BE' : 'beige',
            }}
          >
            scroll me
          </div>{' '}
          <div
            onClick={() => this.swithState()}
            className="col-4 color--turquoise"
            style={{
              backgroundColor: this.state.backgroundColor
                ? '#45ADA8'
                : 'turquoise',
            }}
          >
            click me
          </div>{' '}
        </div>
        <div className="row">
          <div
            onClick={() => this.swithState()}
            className="col-4 color--turquoise"
            style={{
              backgroundColor: this.state.backgroundColor
                ? '#45ADA8'
                : 'turquoise',
            }}
          >
            click me
          </div>{' '}
          <div
            className="col-4 color--pink"
            style={{
              backgroundColor: this.state.backgroundColorTimer
                ? '#B38184'
                : 'pink',
            }}
          >
            wait 10 sec
          </div>{' '}
          <div
            onScroll={() => this.handleScroll()}
            className="col-4 color--beige"
            style={{
              backgroundColor: this.state.transform ? '#F7E4BE' : 'beige',
            }}
          >
            scroll me
          </div>{' '}
        </div>
        <div className="row">
          <div
            onScroll={() => this.handleScroll()}
            className="col-4 color--beige"
            style={{
              backgroundColor: this.state.transform ? '#F7E4BE' : 'beige',
            }}
          >
            scroll me
          </div>{' '}
          <div
            onClick={() => this.swithState()}
            className="col-4 color--turquoise"
            style={{
              backgroundColor: this.state.backgroundColor
                ? '#45ADA8'
                : 'turquoise',
            }}
          >
            click me
          </div>{' '}
          <div
            className="col-4 color--pink"
            style={{
              backgroundColor: this.state.backgroundColorTimer
                ? '#B38184'
                : 'pink',
            }}
          >
            wait 10 sec
          </div>{' '}
        </div>
      </div>
    );
  }
}

export default App;
